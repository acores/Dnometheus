FROM prom/prometheus:v2.19.2

USER root

RUN mkdir -p /data
RUN chown -R root:root /etc/prometheus /data

WORKDIR /data
ENTRYPOINT ["/bin/prometheus"]
CMD         ["--config.file=/etc/prometheus/prometheus.yml", \
            "--storage.tsdb.path=/data", \
            "--storage.tsdb.retention.time=69d", \
            "--web.enable-lifecycle" ]

COPY prometheus_local_file.yml /etc/prometheus/prometheus.yml
COPY targets/targets_local_file.yml /etc/prometheus/targets/targets.yml
